//
//  Message.h
//  mbraceCase
//
//  Created by Marco Argiolas on 02/04/14.
//  Copyright (c) 2014 Marco Argiolas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class IDText;

@interface Message : NSManagedObject

@property (nonatomic, retain) NSString * textString;
@property (nonatomic, retain) IDText *textToId;

@end
