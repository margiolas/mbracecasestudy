//
//  AppDelegate.m
//  mbraceCase
//
//  Created by Marco Argiolas on 02/04/14.
//  Copyright (c) 2014 Marco Argiolas. All rights reserved.
//

#import "AppDelegate.h"
#import "IDText.h"
#import "Message.h"

@implementation AppDelegate

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
//    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
//    // Override point for customization after application launch.
//    self.window.backgroundColor = [UIColor whiteColor];
//    [self.window makeKeyAndVisible];
    [self createDataBase];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
             // Replace this implementation with code to handle the error appropriately.
             // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        } 
    }
}

- (void)createDataBase
{
    NSArray *notes =  @[@{@"id":@1, @"text": @"First note"},
                        @{@"id":@2, @"text": @"Secon note with a link to http://www.google.de"},
                        @{@"id":@3, @"text": @"Third note"},
                        @{@"id":@4, @"text": @"Fourth note"},
                        @{@"id":@5, @"text": @"Fifth note with an email adress to jakob@mbraceapp.com"}, @{@"id":@6, @"text": @"6th note"},
                        @{@"id":@6, @"text": @"6th note updated"},
                        @{@"id":@7, @"text": @"7th note"},
                        @{@"id":@8, @"text": @"8th note"},
                        @{@"id":@9, @"text": @"9th note"},
                        @{@"id":@10, @"text": @"10th note"},
                        @{@"id":@11, @"text": @"11th note"},
                        @{@"id":@12, @"text": @"12th note"},
                        @{@"id":@13, @"text": @"13th note"},
                        @{@"id":@14, @"text": @"14th note"},
                        @{@"id":@15, @"text": @"get mbrace at http://www.getmbrace.com"},
                        @{@"id":@16, @"text": @"16th note"},
                        @{@"id":@17, @"text": @"17th note"},
                        @{@"id":@18, @"text": @"18th note"},
                        @{@"id":@19, @"text": @"19th note"},
                        @{@"id":@20, @"text": @"20th note"},
                        @{@"id":@21, @"text": [NSNull null]},
                        @{@"id":@22, @"text": @"22th note"},
                        @{@"id":@23, @"text": @"23th note"},
                        @{@"id":@24, @"text": @"Visit www.mbraceapp.com"},
                        @{@"id":@25, @"text": @"25th note"},
                        @{@"id":@26, @"text": @"Note that is a little bit longer than all the other notes because of consiting of some strings that are useless and take a lot of space"}, @{@"id":@27, @"text": @"27th note"}, @{@"id":@28, @"text": @"28th note"}, @{@"id":@29, @"text": @"29th note"},
                        @{@"id":@30, @"text": @"another email to lukas@mbraceapp.com"}, @{@"id":@31, @"text": @"31th note"},
                        @{@"id":@32, @"text": @"32th note"},
                        @{@"id":@33, @"text": @"33th note"},
                        @{@"id":@34, @"text": @"almost at the end note"}, @{@"id":@35, @"text": @"Last note note"}, @{@"id":@12, @"text": @"Updated 12th note"}];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        for (int i = 0; i < [notes count]; i++)
        {
            NSDictionary *dict = [notes objectAtIndex:i];
            NSString *idstring;
            NSString *textString;
            if([dict objectForKey:@"id"] != nil)
            {
                idstring = [NSString stringWithFormat:@"%@", [dict objectForKey:@"id"]];
            }
            if([dict objectForKey:@"text"] != nil)
            {
                textString = [NSString stringWithFormat:@"%@", [dict objectForKey:@"text"]];
            }
            // Grab the context
            NSManagedObjectContext *context = [self managedObjectContext];
            
            // Grab the Label entity
            IDText *ent = [NSEntityDescription insertNewObjectForEntityForName:@"IDText" inManagedObjectContext:context];
            ent.idString = idstring;
            Message *mess = [NSEntityDescription insertNewObjectForEntityForName:@"Message" inManagedObjectContext:context];
            mess.textString = textString;
            ent.idToText = mess;
            mess.textToId = ent;

            NSError *error = nil;
            if ([context save:&error])
            {
                NSLog(@"The save was successful!");
            } else {
                NSLog(@"The save wasn't successful: %@", [error userInfo]);
            }
        }
        [[NSNotificationCenter defaultCenter]postNotificationName:@"DB_CREATED" object:nil];
    });
}

- (NSMutableDictionary*) readDatabase
{
    // Construct a fetch request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"IDText" inManagedObjectContext:self.managedObjectContext];
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    [fetchRequest setEntity:entity];
    NSError *error = nil;
    NSArray *fetchedObjects = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    for (IDText *ent in fetchedObjects)
    {
        NSString *idString = ent.idString;
        NSString *textString = ent.idToText.textString;
        [dictionary setObject:textString forKey:idString];
    }

    return dictionary;
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"mbraceCase" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"mbraceCase.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
         if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }    
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

@end
