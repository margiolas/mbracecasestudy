//
//  IDText.h
//  mbraceCase
//
//  Created by Marco Argiolas on 02/04/14.
//  Copyright (c) 2014 Marco Argiolas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Message;

@interface IDText : NSManagedObject

@property (nonatomic, retain) NSString * idString;
@property (nonatomic, retain) Message *idToText;

@end
