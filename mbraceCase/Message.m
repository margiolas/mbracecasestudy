//
//  Message.m
//  mbraceCase
//
//  Created by Marco Argiolas on 02/04/14.
//  Copyright (c) 2014 Marco Argiolas. All rights reserved.
//

#import "Message.h"
#import "IDText.h"


@implementation Message

@dynamic textString;
@dynamic textToId;

@end
