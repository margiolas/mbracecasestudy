//
//  MainTableViewController.m
//  mbraceCase
//
//  Created by Marco Argiolas on 02/04/14.
//  Copyright (c) 2014 Marco Argiolas. All rights reserved.
//

#import "MainTableViewController.h"

@implementation MainTableViewCell

@synthesize textView;

@end
@interface MainTableViewController ()

@end

@implementation MainTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadDB:) name:@"DB_CREATED" object:nil];

}

-(void)loadDB:(id)sender
{
    AppDelegate *appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    dbDictionary = [appdelegate readDatabase];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
    });

}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [dbDictionary count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *mainCellIdentifier = @"MainTableViewCellID";
    MainTableViewCell *cell = (MainTableViewCell*)[tableView dequeueReusableCellWithIdentifier:mainCellIdentifier];
    
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MainTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    else
    {
        cell.textView.text = @"";
        [cell.textView setFrame:CGRectMake(0, 10, 320, 45)];
        [cell.textView setTextColor:[UIColor blackColor]];
    }
    cell.textView.text = [dbDictionary objectForKey:[NSString stringWithFormat:@"%d", indexPath.row + 1]];
    cell.textView.userInteractionEnabled = NO;
    
    NSMutableAttributedString *attributedString;
    NSDataDetector *linkDetector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink error:nil];
    NSArray *matches = [linkDetector matchesInString:cell.textView.text options:0 range:NSMakeRange(0, [cell.textView.text length])];
    if([matches count] == 0)
    {
        [attributedString removeAttribute:NSLinkAttributeName range:[cell.textView.text rangeOfString:cell.textView.text]];
        [attributedString removeAttribute:NSForegroundColorAttributeName range:[cell.textView.text rangeOfString:cell.textView.text]];
        cell.textView.attributedText = attributedString;
    }
    for (NSTextCheckingResult *match in matches)
    {
        if ([match resultType] == NSTextCheckingTypeLink)
        {
            [cell.textView setTextColor:[UIColor blueColor]];
            NSRange isRange = [cell.textView.text rangeOfString:@"http" options:NSCaseInsensitiveSearch];
            NSString *urlString = [[match URL] absoluteString];
            if(isRange.length == 0)
            {
                //http or https not present in string -> remove from urlString
                NSArray *array = [urlString componentsSeparatedByString:@"//"];
                if([array count] > 1)
                    urlString = [array objectAtIndex:1];
            }
            
            [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor blueColor] range:NSMakeRange(0,[cell.textView.text length])];
            
            
            [attributedString addAttribute:NSLinkAttributeName value:[UIColor blueColor] range:[cell.textView.text rangeOfString:urlString]];
            cell.textView.selectable = NO;
            
            
            cell.textView.attributedText = attributedString;
            cell.textView.userInteractionEnabled = YES;
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(textTapped:)];
            [cell.textView addGestureRecognizer:tap];
            int textViewTag = (indexPath.row*10)+ indexPath.section;
            [cell.textView setTag:textViewTag];
        }
    }
    cell.textView.text = [dbDictionary objectForKey:[NSString stringWithFormat:@"%d", indexPath.row + 1]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *string = [dbDictionary objectForKey:[NSString stringWithFormat:@"%d", indexPath.row + 1]];
    NSDataDetector *linkDetector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink error:nil];
    NSArray *matches = [linkDetector matchesInString:string options:0 range:NSMakeRange(0, [string length])];
   
    NSString *checkEmailString = nil;
    checkEmailString = [self checkEmail:string];
    if(checkEmailString != nil)
    {
        MFMailComposeViewController *controller = [[MFMailComposeViewController alloc] init];
        controller.mailComposeDelegate = self;
        [controller setToRecipients:[NSArray arrayWithObject:checkEmailString]];
        [self presentViewController:controller animated:YES completion:nil];
    }
    else
    {
        for (NSTextCheckingResult *match in matches)
        {
            if ([match resultType] == NSTextCheckingTypeLink)
            {
                NSRange isRange = [string rangeOfString:@"http" options:NSCaseInsensitiveSearch];
                NSString *urlString = [[match URL] absoluteString];
                if(isRange.length == 0)
                {
                    //http or https not present in string -> remove from urlString
                    NSArray *array = [urlString componentsSeparatedByString:@"//"];
                    urlString = [array objectAtIndex:1];
                }
                
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
            }
        }
    }
}

- (void)textTapped:(UITapGestureRecognizer *)recognizer
{
    UITextView *textView = (UITextView *)recognizer.view;
    CGPoint location = [recognizer locationInView:textView];
    
    NSDataDetector *linkDetector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink error:nil];
    NSArray *matches = [linkDetector matchesInString:textView.text options:0 range:NSMakeRange(0, [textView.text length])];
    NSRange urlRange;
    NSString *urlString;
    for (NSTextCheckingResult *match in matches)
    {
        if ([match resultType] == NSTextCheckingTypeLink)
        {
            urlString = [[match URL] absoluteString];
            NSRange isRange = [textView.text rangeOfString:@"http" options:NSCaseInsensitiveSearch];
            NSString *urlString = [[match URL] absoluteString];
            if(isRange.length == 0)
            {
                //http or https not present in string -> remove from urlString
                NSArray *array = [urlString componentsSeparatedByString:@"//"];
                urlString = [array objectAtIndex:1];
            }
            
            urlRange = [textView.text rangeOfString:urlString];
        }
    }
    
    NSUInteger characterIndex;
    
    location.x -= textView.textContainerInset.left;
    location.y -= textView.textContainerInset.top;
    NSLayoutManager *layoutManager = textView.layoutManager;
    characterIndex = [layoutManager characterIndexForPoint:location inTextContainer:textView.textContainer fractionOfDistanceBetweenInsertionPoints:NULL];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
}

-(NSString*) checkEmail: (NSString *) candidate
{
    NSString *returnString = nil;
    NSArray *arrComponents = [candidate componentsSeparatedByString:@" "];
    for (int i = 0; i < [arrComponents count]; i++)
    {
        if([[arrComponents objectAtIndex:i] rangeOfString:@"@"].location != NSNotFound)
            returnString = [arrComponents objectAtIndex:i];
    }
    if(returnString == nil)
    {
        NSArray *arrComponents2 = [candidate componentsSeparatedByString:@":"];
        for(int i = 0; i < [arrComponents2 count]; i++)
        {
            if([[arrComponents2 objectAtIndex:i] rangeOfString:@"@"].location != NSNotFound)
                returnString = [arrComponents2 objectAtIndex:i];
        }
    }
    return returnString;
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
