//
//  IDText.m
//  mbraceCase
//
//  Created by Marco Argiolas on 02/04/14.
//  Copyright (c) 2014 Marco Argiolas. All rights reserved.
//

#import "IDText.h"
#import "Message.h"


@implementation IDText

@dynamic idString;
@dynamic idToText;

@end
