//
//  MainTableViewController.h
//  mbraceCase
//
//  Created by Marco Argiolas on 02/04/14.
//  Copyright (c) 2014 Marco Argiolas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface MainTableViewCell: UITableViewCell
{
    IBOutlet UITextView *textView;
}

@property (strong, nonatomic) IBOutlet UITextView *textView;

@end
@interface MainTableViewController : UITableViewController <MFMailComposeViewControllerDelegate>
{
    NSMutableDictionary *dbDictionary;
}

@end
